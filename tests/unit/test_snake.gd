extends "res://addons/gut/test.gd"


func before_each():
  gut.p("ran setup", 2)

func after_each():
  gut.p("ran teardown", 2)

func before_all():
  gut.p("ran run setup", 2)

func after_all():
  gut.p("ran run teardown", 2)


class TestSnake:
  extends "res://addons/gut/test.gd"
  
  var Obj = load('res://src/snake.gd')
  var _obj = null

  func before_all():
    gut.p('TestSnake:  pre-run')

  func before_each():
    _obj = Obj.new()
    _obj._ready()
    gut.p('TestSnake:  setup')

  func after_each():
    gut.p('TestSnake:  teardown')

  func after_all():
    gut.p('TestSnake:  post-run')

  func test_snake_exists():
    assert_true(_obj.exists(), "should exist") 

  func test_position_is_zero_zero():
    assert_eq(_obj.position, Vector2(0,0), "should be the same position")

  func test_velocity_is_1_0():
    assert_eq(_obj.velocity, Vector2(1, 0), "should be moving upwards")

  func test_after_one_second_position_is_1_0():
    gut.simulate(_obj, 1, 1)
    assert_eq(_obj.position, Vector2(1,0), "time should move the snake upwards")

  func test_after_point_one_second_position_is_point1_0():
    gut.simulate(_obj, 1, .1)
    assert_eq(_obj.position, Vector2(0.1,0), "time should move the snake upwards")

  func test_after_point_two_seconds_position_is_point2_0():
    gut.simulate(_obj, 2, .1)
    assert_eq(_obj.position, Vector2(0.2,0), "time should move the snake upwards")

  func test_if_velocity_changes_to_0_m1_then_position_becomes_p2_mp2():
    gut.simulate(_obj, 2, .1)
    _obj.set_direction(Vector2(0, -1))
    gut.simulate(_obj, 2, .1)
    assert_eq(_obj.position, Vector2(0.2,-0.2), "time should move the snake upwards then leftwards")

  func test_snake_starts_with_4_segments():
    assert_eq(_obj.length(), 4)

  func test_grow_increments_snake_length():
    _obj.grow()
    assert_eq(_obj.length(), 5)

  func test_when_snake_has_5_segments_grow_increments_snake_length_to_6():
    _obj.grow()
    _obj.grow()
    assert_eq(_obj.length(), 6)

  func test_snake_0_segment_object_is_below_head():
    assert_eq(_obj.segments[0].position, Vector2(-1, 0))

  func test_snake_1_segment_object_is_below_head():
    assert_eq(_obj.segments[1].position, Vector2(-2, 0))

  func test_snake_2_segment_object_is_below_head():
    assert_eq(_obj.segments[2].position, Vector2(-3, 0))

  func test_when_head_moves_so_does_segment_0():
    gut.simulate(_obj, 1, 1)
    assert_eq(_obj.segments[0].position, Vector2(0, 0))

  func test_setting_segment_to_change_direction_after_delay_happens():
    # at position 0,0 set the velocity to be 0, 1
    _obj.segments[0].set_direction(Vector2(0, 0), Vector2(0, 1))
    gut.simulate(_obj, 1, 1) # move it to 0,0
    assert_eq(_obj.segments[0].velocity, Vector2(0, 1))

  func test_when_head_changes_direction_segment0_does_not_change_direction():
    _obj.set_direction(Vector2(0,1))
    assert_eq(_obj.segments[0].velocity, Vector2(1,0))

  func test_when_head_changes_direction_segment0_changes_direction_when_gets_to_where_head_was():
    _obj.set_direction(Vector2(0,1))
    gut.simulate(_obj, 1, 1)
    assert_eq(_obj.segments[0].velocity, Vector2(0, 1))

  func test_when_head_changes_direction_to_m1_0_segment0_changes_direction_to_m1_0():
    _obj.set_direction(Vector2(-1,0))
    gut.simulate(_obj, 1, 1)
    assert_eq(_obj.segments[0].velocity, Vector2(-1, 0))

  func test_when_segment_changes_direction_queue_is_reduced_by_one_to_zero():
    _obj.set_direction(Vector2(0,1))
    gut.simulate(_obj, 1, 1)
    assert_eq(_obj.segments[0].queue.size(),0)

  func test_when_segment_passes_through_turning_point_then_turns():
    var seg = load("res://src/segment.gd").new()
    seg.set_direction(Vector2(1, 0),Vector2(0,1))
    gut.simulate(seg, 10, 0.1)
    assert_eq(seg.queue.size(), 0)

  func test_when_segment_passes_through_turning_point_then_position_is_correct():
    _obj.set_direction(Vector2(0,1))
    gut.simulate(_obj, 11, 0.1)
    assert_eq(_obj.segments[0].position.snapped(Vector2(0.1,0.1)), Vector2(0, 0.1))

  func test_when_segment_changes_direction_queue_of_2_is_reduced_to_1():
    _obj.set_direction(Vector2(0,1))
    gut.simulate(_obj, 5, 0.1)
    _obj.set_direction(Vector2(1, 0))
    gut.simulate(_obj, 5, 0.1)
    assert_eq(_obj.segments[0].queue.size(),1)

  func test_when_segment_changes_direction_twice_correct_vector2_left():
    _obj.set_direction(Vector2(0,1))
    gut.simulate(_obj, 10, 0.1)
    _obj.set_direction(Vector2(1, 0))
    var queue = _obj.segments[0].queue
    assert_eq(queue[0][0].snapped(Vector2(0.1, 0.1)), Vector2(0, 1))
    assert_eq(queue[0][1].snapped(Vector2(0.1, 0.1)), Vector2(1, 0))

  func test_segment1_gets_delayed_queue_item():
    _obj.set_direction(Vector2(0,1))
    assert_eq(_obj.segments[1].queue, [[Vector2(0,0), Vector2(0,1)]])
 
  func test_has_4_children():
    assert_eq(_obj.get_child_count(), 4)

class TestDrawing:
  extends "res://addons/gut/test.gd"
  
  func before_all():
    pass

  func before_each():
    pass

  func test_snake_with_no_segments_draw_calls_draw_snake_with_one_point():
    var snake = partial_double('res://src/snake.gd').new()
    snake.segments = []
    snake._draw()
    var expected = PoolVector2Array()
    expected.append(Vector2(0, 0))
    assert_called(snake, 'draw_snake', [expected])

  func test_draw_calls_draw_snake_with_array_length_5():
    var snake = partial_double('res://src/snake.gd').new()
    snake._ready()
    snake._draw()
    var expected = PoolVector2Array()
    expected.append(Vector2(0,0))
    expected.append(Vector2(-1,0))
    expected.append(Vector2(-2,0))
    expected.append(Vector2(-3,0))
    expected.append(Vector2(-4,0))
    assert_called(snake, 'draw_snake', [expected])
