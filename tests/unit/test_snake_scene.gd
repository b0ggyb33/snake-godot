extends "res://addons/gut/test.gd"

var scene = null

func before_each():
  scene = load('res://scenes/snake.tscn').instance()

func test_scene_root_is_KinematicBody2D():
  assert_eq(scene.get_class(), "KinematicBody2D")

func test_scene_script_is_snake():
  assert_eq(scene.get_script().get_path(), "res://src/snake.gd")

func test_scene_has_a_child():
  assert_eq(scene.get_child_count(), 1)
  
func test_has_a_controller_child():
  assert_eq(scene.get_children()[0].get_class(), "Node")

func test_controller_node_has_keyboard_script():
  assert_eq(scene.get_children()[0].get_script().get_path(), "res://src/keyboard.gd")

