extends "res://addons/gut/test.gd"

var scene = null

func before_each():
  scene = load('res://main.tscn').instance()

func test_scene_root_is_Node2D():
  assert_eq(scene.get_class(), "Node2D")

func test_scene_has_three_children():
  assert_eq(scene.get_child_count(), 3)
  
func test_first_scene_child_is_a_camera():
  assert_eq(scene.get_children()[0].get_class(), "Camera2D")

func test_camera_is_active():
  assert_true(scene.get_children()[0].current)

func test_second_scene_child_is_a_KinematicBody2D():
  assert_eq(scene.get_children()[1].get_class(), "KinematicBody2D")

func test_second_scene_child_is_a_node():
  assert_eq(scene.get_children()[2].get_class(), "Node")

func test_second_scene_child_has_orchard_script():
  assert_eq(scene.get_children()[2].get_script().get_path(), "res://src/orchard.gd")
