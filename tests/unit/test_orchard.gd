extends "res://addons/gut/test.gd"

func before_each():
  gut.p("ran setup", 2)

func after_each():
  gut.p("ran teardown", 2)

func before_all():
  gut.p("ran run setup", 2)

func after_all():
  gut.p("ran run teardown", 2)

class TestOrchard:
  extends "res://addons/gut/test.gd"

  var Obj = load('res://src/orchard.gd')
  var orchard = null

  func before_each():
    orchard = Obj.new()

  func test_no_apples_exist_at_start():
    assert_eq(orchard.get_child_count(), 0)

  func test_after_two_seconds_there_is_an_apple():
    gut.simulate(orchard, 20, 0.1)
    assert_eq(orchard.get_child_count(), 1)
  
  func test_after_three_seconds_there_is_an_apple():
    gut.simulate(orchard, 30, 0.1)
    assert_eq(orchard.get_child_count(), 1)

  func test_after_four_seconds_there_are_two_apples():
    gut.simulate(orchard, 40, 0.1)
    assert_eq(orchard.get_child_count(), 2)

class TestRNG:
  extends "res://addons/gut/test.gd"

  func test_new_apple_calls_get_random_position_once():
    var pd_orchard = partial_double('res://src/orchard.gd').new()

    gut.simulate(pd_orchard, 20, 0.1) # make one apple
    assert_call_count(pd_orchard, 'get_random_position', 1)

  func test_new_apple_has_the_return_value_of_rng():
    var pd_orchard = partial_double('res://src/orchard.gd').new()
    stub(pd_orchard, 'get_random_position').to_return(Vector2(42, 24))

    gut.simulate(pd_orchard, 20, 0.1) # make one apple
    assert_eq(pd_orchard.get_children()[0].position, Vector2(42,24))

  func test_second_apple_has_a_different_return_value_of_rng():
    var pd_orchard = partial_double('res://src/orchard.gd').new()
    stub(pd_orchard, 'get_random_position').to_return(Vector2(42, 24))
    gut.simulate(pd_orchard, 20, 0.1) # make one apple
    stub(pd_orchard, 'get_random_position').to_return(Vector2(34, 78))
    gut.simulate(pd_orchard, 20, 0.1) # make one apple
    assert_eq(pd_orchard.get_children()[1].position, Vector2(34, 78))

class TestInteractionWithSprite:
  extends "res://addons/gut/test.gd"

  var Obj = load('res://src/orchard.gd')
  var orchard = null

  func before_each():
    orchard = Obj.new()
  
  func test_making_a_new_apple_adds_an_apple_as_a_child():
    gut.simulate(orchard, 20, 0.1) # make an apple
    assert_eq(orchard.get_child_count(), 1)

  func test_second_child_is_an_apple():
    gut.simulate(orchard, 20, 0.1) # make an apple
    assert_eq(orchard.get_children()[0].get_class(), "KinematicBody2D")

func test_after_four_seconds_there_is_one_apple():
  var Obj = load('res://src/orchard.gd')
  var orchard = Obj.new(4)
  gut.simulate(orchard, 40, 0.1)
  assert_eq(orchard.get_child_count(), 1)
  
