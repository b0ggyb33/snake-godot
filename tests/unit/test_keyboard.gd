extends "res://addons/gut/test.gd"

class StubNode2D:
  extends Node2D
  var direction = Vector2(1, 0)
  
  func set_direction(new_direction):
    direction = new_direction

class TestKeyboard:
  extends "res://addons/gut/test.gd"
  
  var Obj = load('res://src/keyboard.gd')
  var _keyboard = null
  var parent = null

  func make_keypress(key):
    var ev = InputEventKey.new()
    ev.set_scancode(key)
    return ev

  func before_each():
    _keyboard = Obj.new()
    parent = StubNode2D.new()
    parent.add_child(_keyboard)

  func test_has_reference_to_parent():
    assert_ne(_keyboard.get_parent(), null)
  
  func test_parent_is_stub_node():
    assert_ne(_keyboard.get_parent().get_class(), "StubNode2D")

  func test_current_direction_is_up():
    assert_eq(_keyboard.direction, Vector2(0, -1))

  func test_sending_left_sets_direction_left():
    _keyboard._input(make_keypress(KEY_A))
    assert_eq(_keyboard.direction, Vector2(-1, 0))

  func test_sending_right_sets_direction_right():
    _keyboard._input(make_keypress(KEY_D))
    assert_eq(_keyboard.direction, Vector2(1, 0))

  func test_sending_up_sets_direction_up():
    _keyboard.direction = Vector2(1, 0) # set to right
    _keyboard._input(make_keypress(KEY_W))
    assert_eq(_keyboard.direction, Vector2(0, -1))

  func test_sending_down_sets_direction_down():
    _keyboard.direction = Vector2(1, 0) # set to right
    _keyboard._input(make_keypress(KEY_S))
    assert_eq(_keyboard.direction, Vector2(0, 1))

  func test_sending_left_does_not_happen_when_direction_is_right():
    _keyboard.direction = Vector2(1, 0)
    _keyboard._input(make_keypress(KEY_A))
    assert_eq(_keyboard.direction, Vector2(1, 0))

  func test_sending_right_does_not_happen_when_direction_is_left():
    _keyboard.direction = Vector2(-1, 0)
    _keyboard._input(make_keypress(KEY_D))
    assert_eq(_keyboard.direction, Vector2(-1, 0))

  func test_sending_up_does_nothing_when_when_direction_is_down():
    _keyboard.direction = Vector2(0, 1)
    _keyboard._input(make_keypress(KEY_W))
    assert_eq(_keyboard.direction, Vector2(0, 1))

  func test_sending_down_does_nothing_when_when_direction_is_up():
    _keyboard.direction = Vector2(0, -1)
    _keyboard._input(make_keypress(KEY_S))
    assert_eq(_keyboard.direction, Vector2(0, -1))

  func test_sets_direction_on_parent():
    _keyboard._input(make_keypress(KEY_A))
    assert_eq(parent.direction, Vector2(-1, 0))

  func test_only_handles_keyboard():
    var ev = InputEventMouseMotion.new()
    _keyboard._input(ev)
    assert_true(true)
