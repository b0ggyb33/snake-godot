extends 'res://addons/gut/test.gd'

func test_has_collider_shape():
  var apple = load('res://scenes/apple.tscn').instance()
  assert_eq(apple.get_children()[0].get_shape().get_radius(), 1)
