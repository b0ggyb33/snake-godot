extends KinematicBody2D

var velocity = Vector2(1, 0)
var segments = []
var Segment = preload('res://src/segment.gd')

func _ready():
  var pos = position
  for i in range(4):
    pos -= velocity
    var newseg = Segment.new()
    newseg.position = pos
    segments.append(newseg)
    add_child(newseg)
  return

func _physics_process(delta):
  translate(velocity*delta)
  update()

func exists():
  return true

func grow():
  segments.append(Segment.new())

func length():
  return segments.size()

func set_direction(v):
  velocity = v
  for i in range(segments.size()):
    segments[i].set_direction(position, v)

func _draw():
  var array = PoolVector2Array()
  array.append(position)
  for i in segments:
    array.append(i.position)
  draw_snake(array)
  draw_polyline(array, Color.yellow)

func draw_snake(array):
  pass
