extends Node

var directions = {'left':Vector2(-1,0), 'right': Vector2(1, 0), 'down':Vector2(0, 1), 'up':Vector2(0, -1)}
var direction = directions['up']

func _input(event):

  if event.get_class() == "InputEventKey":
    match event.get_scancode():
      KEY_A:
        if direction != directions['right']:
          direction = directions['left']
      KEY_D:
        if direction != directions['left']:
          direction = directions['right']
      KEY_W:
        if direction != directions['down']:
          direction = directions['up']
      KEY_S:
        if direction != directions['up']:
          direction = directions['down']
    get_parent().set_direction(direction)
