extends Node2D


var queue = []
var velocity = Vector2(1, 0)

func _physics_process(delta):
  translate(velocity*delta)
  check_for_direction_changes()

func set_direction(pos, val):
  queue.append([pos, val]) 

func check_for_direction_changes():
  if queue.size() > 0:
    if queue[0][0].snapped(Vector2(0.1, 0.1)) == position.snapped(Vector2(0.1, 0.1)):
      position = queue[0][0]
      velocity = queue[0][1]
      queue.pop_front()
  
