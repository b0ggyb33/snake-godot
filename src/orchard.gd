extends Node

var time_since_last_apple = 0
var time_between_apples = 0
const X_SIZE = 100
const Y_SIZE = 100

func _init(appletime=2):
  time_between_apples = appletime

func get_random_position():
  var x = rand_range(1, X_SIZE)
  var y = rand_range(1, Y_SIZE)
  return Vector2(x, y)

func _process(delta):
  time_since_last_apple += delta
  
  if time_since_last_apple >= time_between_apples:
	var apple = load("res://scenes/apple.tscn").instance()
	apple.position = get_random_position()
	add_child(apple)
	time_since_last_apple = 0



