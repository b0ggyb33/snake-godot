FROM ubuntu:latest

RUN apt update && apt upgrade -yq
RUN apt install -yq ansible unzip

COPY playbook.yml /opt/
WORKDIR /opt/
RUN ansible-playbook playbook.yml

COPY . /opt/
ENTRYPOINT ["./run-tests.sh"]
