use gdnative::prelude::*;
use rand::Rng;

#[methods]
impl Orchard {
    fn new(_owner: &Node2D) -> Self {
        Orchard {
            apples: Vec::new(),
            generator: rand::thread_rng(),
            time_since: 0.0,
            x: 16,
            y: 16,
            apple_time: 2.0,
        }
    }

    #[export]
    fn _ready(&self, _owner: &Node2D) {
        godot_print!("New orchard");
    }

    #[export]
    fn _process(&mut self, _owner: &Node2D, delta_t: f64) {
        self.update(delta_t);
        _owner.update();
    }

    #[export]
    fn _draw(&self, _owner: &Node2D) {
        for item in &self.apples {
            let pos: Vector2 = Vector2::new(item.position.0 as f32, item.position.1 as f32);
            CanvasItem::draw_circle(_owner, pos, 0.1, Color::rgb(1.0, 0.0, 0.0))
        }
    }
}

#[derive(Debug, NativeClass)]
#[inherit(Node2D)]
pub struct Orchard {
    apples: Vec<Apple>,
    generator: rand::rngs::ThreadRng,
    x: i8,
    y: i8,
    time_since: f64,
    apple_time: f64,
}

impl Orchard {
    fn make_apple(&mut self) {
        let a = Apple {
            position: (
                self.generator.gen_range(-self.x, self.x),
                self.generator.gen_range(-self.y, self.y),
            ),
        };
        godot_print!("new apple: {}, {}", a.position.0, a.position.1);
        self.apples.push(a);
    }

    fn update(&mut self, delta_t: f64) {
        self.time_since += delta_t;
        if self.time_since > self.apple_time {
            self.make_apple();
            self.time_since = 0.0;
        }
    }
}

#[derive(Debug, NativeClass)]
#[inherit(gdnative::api::Area2D)]
struct Apple {
    position: (i8, i8),
}

#[methods]
impl Apple {
    fn new(_owner: &gdnative::api::Area2D) -> Self {
        Apple { position: (0, 0) }
    }

    #[export]
    fn _ready(&self, _owner: &gdnative::api::Area2D) {
        godot_print!("new apple");
        _owner.translate(Vector2::new(self.position.0 as f32, self.position.1 as f32));
        let shape = gdnative::api::CircleShape2D::new();
        shape.set_radius(1.0);
        let circle = gdnative::api::CollisionShape2D::new();
        circle.set_shape(shape);
        _owner.add_child(circle, false);
    }

    fn set_position(&mut self, position: (i8, i8)) {
        self.position = position;
    }
}
