use gdnative::prelude::*;
use std::collections::VecDeque;

#[derive(Debug)]
enum Direction {
    Left,
    Up,
    Down,
    Right,
}

const fn get_direction(direction: &Direction) -> (i8, i8) {
    match direction {
        Direction::Right => (1, 0),
        Direction::Down => (0, 1),
        Direction::Left => (-1, 0),
        Direction::Up => (0, -1),
    }
}

#[derive(Debug, NativeClass)]
#[inherit(Node2D)]
pub struct Snake {
    position: VecDeque<(i8, i8)>,
    grow: bool,
    snake_time: f64,
    time_since: f64,
    direction: Direction,
}

#[methods]
impl Snake {
    fn new(_owner: &Node2D) -> Self {
        let mut s = Snake::rust_new();
        s.grow();
        s
    }

    #[export]
    fn _ready(&self, _owner: &Node2D) {
        godot_print!("New Snake!");
    }

    #[export]
    fn _process(&mut self, _owner: &Node2D, delta_t: f64) {
        self.time_since += delta_t;
        let input = Input::godot_singleton();
        if input.is_action_pressed("move_left") {
            self.set_direction(Direction::Left);
        }
        if input.is_action_pressed("move_right") {
            self.set_direction(Direction::Right);
        }
        if input.is_action_pressed("move_up") {
            self.set_direction(Direction::Up);
        }
        if input.is_action_pressed("move_down") {
            self.set_direction(Direction::Down);
        }
        if self.time_since >= self.snake_time {
            self.slither();
            _owner.update();
            self.time_since = 0.0;
        }
    }

    #[export]
    fn _draw(&self, _owner: &Node2D) {
        let mut v: Vec<Vector2> = Vec::new();

        godot_print!("draw");
        for item in &self.position {
            godot_print!("snake element: {}, {}", item.0, item.1);
            v.push(Vector2::new(item.0.into(), item.1.into()));
        }

        CanvasItem::draw_polyline(
            _owner,
            TypedArray::from_vec(v),
            Color::rgb(0.0, 1.0, 0.0),
            1.0,
            false,
        );
    }
}

impl Snake {
    fn rust_new() -> Self {
        let mut v = VecDeque::new();
        v.push_back((0, 0));
        Snake {
            position: v,
            grow: false,
            snake_time: 0.5,
            time_since: 0.0,
            direction: Direction::Right,
        }
    }

    fn set_position(&mut self, position: VecDeque<(i8, i8)>) {
        self.position = position;
    }

    fn set_direction(&mut self, direction: Direction) {
        self.direction = direction;
    }

    fn slither(&mut self) {
        let (dx, dy) = get_direction(&self.direction);
        let (x, y) = *self.position.iter().last().unwrap();

        self.position.push_back((x + dx, y + dy));
        if !self.grow {
            self.position.pop_front();
        } else {
            self.grow = false;
        }
    }

    fn grow(&mut self) {
        self.grow = true;
    }
}

#[test]
fn snake_is_at_00() {
    let s = Snake::rust_new();
    assert_eq!(s.position, vec![(0, 0)]);
}

#[test]
fn snake_moves_to_10() {
    let mut s = Snake::rust_new();
    s.slither();
    assert_eq!(s.position, vec![(1, 0)]);
}

#[test]
fn snake_moves_up() {
    let mut s = Snake::rust_new();
    s.set_direction(Direction::Up);
    s.slither();
    assert_eq!(s.position, vec![(0, -1)]);
}

#[test]
fn snake_moves_down() {
    let mut s = Snake::rust_new();
    s.set_direction(Direction::Down);
    s.slither();
    assert_eq!(s.position, vec![(0, 1)]);
}

#[test]
fn snake_moves_to_m10() {
    let mut s = Snake::rust_new();
    s.set_direction(Direction::Left);
    s.slither();
    assert_eq!(s.position, vec![(-1, 0)]);
}

#[test]
fn snake_moves_twice_to_20() {
    let mut s = Snake::rust_new();
    s.slither();
    s.slither();
    assert_eq!(s.position, vec![(2, 0)]);
}

#[test]
fn snake_with_two_segments_adds_to_end() {
    let mut s = Snake::rust_new();
    let mut p = VecDeque::<(i8, i8)>::new();
    p.push_back((0, 0));
    p.push_back((0, 1));
    s.set_position(p);
    s.slither();
    assert_eq!(s.position, vec![(0, 1), (1, 1)]);
}

#[test]
fn on_grow_gains_a_segment() {
    let mut s = Snake::rust_new();
    s.grow();
    s.slither();
    assert_eq!(s.position, vec![(0, 0), (1, 0)]);
}

#[test]
fn move_then_grow_gains_a_different_segment() {
    let mut s = Snake::rust_new();
    s.slither();
    s.grow();
    s.slither();
    assert_eq!(s.position, vec![(1, 0), (2, 0)]);
}

#[test]
fn grow_gains_nothing() {
    let mut s = Snake::rust_new();
    s.grow();
    assert_eq!(s.position, vec![(0, 0)]);
}

#[test]
fn grow_then_move_twice_adds_one() {
    let mut s = Snake::rust_new();
    s.grow();
    s.slither();
    s.slither();
    assert_eq!(s.position, vec![(1, 0), (2, 0)]);
}

#[test]
fn move_up_with_new_twice_is_correct() {
    let mut s = Snake::rust_new();
    let mut p = VecDeque::<(i8, i8)>::new();
    p.push_back((0, 1)); // tail
    p.push_back((0, 0)); // head
    s.set_position(p);
    s.set_direction(Direction::Up);
    s.slither();
    assert_eq!(s.position, vec![(0, 0), (0, -1)]);
    s.slither();
    assert_eq!(s.position, vec![(0, -1), (0, -2)]);
}
