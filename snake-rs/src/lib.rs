use gdnative::prelude::*;
mod orchard;
mod snake;

fn init(handle: InitHandle) {
    handle.add_class::<orchard::Orchard>();
    handle.add_class::<snake::Snake>();
}

godot_init!(init);
